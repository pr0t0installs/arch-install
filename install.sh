#!/bin/bash

if ! [ $(id -u) = 0 ]; then
   echo "The script need to be run as root." >&2
   exit 1
fi

if [ $SUDO_USER ]; then
    USER=$SUDO_USER
else
    USER=$(whoami)
fi

# Initialize flags and variables
USER_HOME=$(eval echo "~$USER")
D_FLAG=false
IS_VM=false
PENTEST=false
PERSONAL=false
DEV=false

# Parse the command-line flags
while getopts "d:v" opt; do
    case $opt in
        d) 
            D_FLAG=true
            PACKAGE_CATEGORY="$OPTARG"  # Store the argument for -d
            ;;
        v) 
            IS_VM=true
            ;;
        *)
            echo "Usage: $0 -d <package_category> [-v]"
            exit 1
            ;;
    esac
done
    
case $PACKAGE_CATEGORY in
    home) 
        PERSONAL=true
	DEV=true
        ;;
    work) 
	PENTEST=true
	DEV=true
	;;
    temp)
	PENTEST=true
        ;;
esac

# Check if required flags are provided
if [ "$D_FLAG" = false ] && [ -z "$PACKAGE_CATEGORY" ]; then
    echo "Error: The -d flag requires a package category argument."
    echo "Usage: $0 -d <package_category> [-vm]"
    exit 1
fi

run_as_user() {
	sudo -u "$USER" "$@"
}

echo "Downloading Packages"
pacman -Syu --noconfirm - < "$USER_HOME/install/arch-install/base_packages"


echo "Done Downloading Packages"

echo "Copying dotfiles"
run_as_user git clone https://gitlab.com/pr0t0installs/dotfiles.git "$USER_HOME/install/dotfiles"
run_as_user git clone https://gitlab.com/pr0t0installs/wallpapers.git "$USER_HOME/install/wallpapers"
run_as_user mkdir "$USER_HOME/.config"
run_as_user rsync -av --exclude=.git "$USER_HOME/install/dotfiles/" "$USER_HOME/.config/"
mv "$USER_HOME/.config/.Xresources" "$USER_HOME/"
mv "$USER_HOME/.config/.bashrc" "$USER_HOME/"
mv "$USER_HOME/.config/.ssh" "$USER_HOME/"
run_as_user mkdir "$USER_HOME/.local/share/applications/"
mv "$USER_HOME/.config/nvim.desktop" "$USER_HOME/.local/share/applications/"

run_as_user git clone https://gitlab.com/pr0t0installs/nvim.git "$USER_HOME/.config/nvim"

run_as_user mkdir "$USER_HOME/Pictures/"
run_as_user mkdir "$USER_HOME/Pictures/Wallpapers/"
run_as_user mkdir "$USER_HOME/Pictures/Screenshots/"
run_as_user rsync -av --exclude=.git "$USER_HOME/install/wallpapers/" "$USER_HOME/Pictures/Wallpapers/"

echo "Installing yay"
run_as_user git clone https://aur.archlinux.org/yay.git "/tmp/yay"
(cd /tmp/yay && run_as_user makepkg -si --noconfirm)

if [ "$IS_VM" = true ]; then
    echo "Downloading Packages for category: VM"
    pacman -S --noconfirm - < "$USER_HOME/install/arch-install/vm_packages"
    systemctl enable spice-vdagentd.service
    systemctl start spice-vdagentd.service
else
    echo "Downloading Packages for category: non-VM"
    pacman -S --noconfirm - < "$USER_HOME/install/arch-install/non_vm_packages"
    echo "Configuring Qemu/kvm"

    systemctl enable libvirtd.service
    systemctl start libvirtd.service

    sed -i 's/# unix_sock_group = "libvirt"/unix_sock_group = "libvirt"/g' /etc/libvirt/libvirtd.conf
    sed -i 's/# unix_sock_rw_perms = "0770"/unix_sock_rw_perms = "0770"/g' /etc/libvirt/libvirtd.conf

    usermod -a -G libvirt $USER
    newgrp libvirt
fi

if [ "$DEV" = true ]; then
    echo "Downloading Packages for category: DEV"
    pacman -S --noconfirm - < "$USER_HOME/install/arch-install/dev_packages"
    run_as_user rustup default stable
fi

if [ "$PENTEST" = true ]; then
    echo "Downloading Packages for category: PENTEST"
    curl -o "/tmp/blackarch.sh" https://blackarch.org/strap.sh
    sh "/tmp/blackarch.sh"
    pacman -S --noconfirm - < "$USER_HOME/install/arch-install/pentest_packages"
fi

echo "Done"

exit
