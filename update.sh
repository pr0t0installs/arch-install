#!/bin/bash

USER=$(whoami)

echo "Updating Packages"

sudo pacman -S - < ~/install/arch-install/base_packages

echo "Done Updating Packages"

su -c $USER

echo "Copying dotfiles"
cd ~/install/dotfiles
git pull
rsync -av --exclude=.git ./ ~/.config/
cd ~/.config/
mv .Xresources ~/
mv .bashrc ~/

rsync -av --exclude=.git ~/install/wallpapers/ ~/Pictures/Wallpapers/

cd ~/.config/nvim/
git pull
